//
//  ViewController.m
//  HelloTextView
//
//  Created by 網際優勢(股)公司Linda Lin on 2015/3/23.
//  Copyright (c) 2015年 uxb2b. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    //在App起始階段，先將doneBtn以及undoBtn設為disable
    _doneBtn.enabled = NO;
    _undoBtn.enabled = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)undoBtnClick:(id)sender {
    //按下Undo時，將文字欄位的內容改為原先iVar:prevText的內容
    _bigTextView.text=prevText;

}

- (IBAction)doneBtnClick:(id)sender {
    //按下Done時
    [_bigTextView resignFirstResponder];
    /*
     *最後因為有使用到UITextViewDelegate故需切換到storeboard
     *檢視個個method畫面裡將TextView Outlet部分的delegate拉到ViewController使其發生關聯
     */
}

- (void)textViewDidBeginEditing:(UITextView *)textView{
    //1.一開始做編輯的時候，先把原本的內容儲存一份起來
    prevText = textView.text;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    //2.在開始編輯時，將undoBtn, doneBtn enable
    _undoBtn.enabled=YES;
    _doneBtn.enabled=YES;
    return YES;
    
}

- (void)textViewDidEndEditing:(UITextView *)textView{
    
    //3.編輯完畢，將兩個按鈕關閉
    _undoBtn.enabled = NO;
    _doneBtn.enabled = NO;
    
}

@end
