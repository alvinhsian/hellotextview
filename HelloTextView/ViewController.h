//
//  ViewController.h
//  HelloTextView
//
//  Created by 網際優勢(股)公司Linda Lin on 2015/3/23.
//  Copyright (c) 2015年 uxb2b. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController<UITextViewDelegate>
{
    NSString *prevText;
}
@property (weak, nonatomic) IBOutlet UIBarButtonItem *doneBtn;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *undoBtn;
- (IBAction)undoBtnClick:(id)sender;
- (IBAction)doneBtnClick:(id)sender;

@property (weak, nonatomic) IBOutlet UITextView *bigTextView;


@end

